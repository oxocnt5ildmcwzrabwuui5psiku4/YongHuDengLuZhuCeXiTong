# 用户登录注册系统
jsp+servlet+JavaBean+MySQL+JDBC
###流程图
![输入图片说明](https://gitee.com/uploads/images/2017/1103/125303_0ebc66ec_1555586.png "登录注册流程图.png")
- 采用MVC模式
![输入图片说明](https://gitee.com/uploads/images/2017/1103/131525_3abf3390_1555586.png "MVC.png")
登录
![输入图片说明](https://gitee.com/uploads/images/2017/1103/131554_bc04b3c5_1555586.png "登录.png")
登录成功
![输入图片说明](https://gitee.com/uploads/images/2017/1103/131609_c6b7e1d9_1555586.png "个人信息.png")
注册
![输入图片说明](https://gitee.com/uploads/images/2017/1103/131621_96de8202_1555586.png "注册.png")
注册成功
![输入图片说明](https://gitee.com/uploads/images/2017/1103/131636_fdba52f0_1555586.png "注册成功.png")