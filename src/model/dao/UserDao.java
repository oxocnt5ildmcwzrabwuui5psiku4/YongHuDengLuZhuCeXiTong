package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;
import model.dao.DataBaseUtil;

public class UserDao {
	/**
	 * 添加用户
	 * @param user 用户对象
	 */
	
	public void saveUser(User user){
		
		Connection conn = DataBaseUtil.getConnection();
		
		String sql = "insert into tb_user(username,password,sex,email,tel) values(?,?,?,?,?)";
		try {
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getSex());
			ps.setString(4, user.getTel());
			ps.setString(5, user.getEmail());
			
			ps.executeUpdate();
			
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
			DataBaseUtil.closeConnection(conn);
		}
	}
	/**
	 * 用户登录
	 * @param username 用户名
	 * @param password 密码
	 * @return 用户对象
	 */
	public User login(String username, String password){
		User user = null;
		
		Connection conn = DataBaseUtil.getConnection();
		
		String sql = "select * from tb_user where username = ? and password = ?";
		try {
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setString(1, username);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				
				user = new User();
				
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setSex(rs.getString("sex"));
				user.setTel(rs.getString("tel"));
				user.setEmail(rs.getString("email"));
			}
			
			rs.close();
			
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
			DataBaseUtil.closeConnection(conn);
		}
		return user;
	}
	/**
	 * 判断用户名在数据库中是否存在
	 * @param username 用户名
	 * @return 布尔值
	 */
	public boolean userIsExist(String username){
		
		Connection conn = DataBaseUtil.getConnection();
		
		String sql = "select * from tb_user where username = ?";
		try {
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setString(1, username);
			
			ResultSet rs = ps.executeQuery();
			
			if(!rs.next()){
				
				return true;
			}
			
			rs.close();
			
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			
			DataBaseUtil.closeConnection(conn);
		}
		return false;
	}
	
	
}

