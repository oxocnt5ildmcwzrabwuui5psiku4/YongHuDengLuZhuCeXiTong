package service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.User;
import model.dao.UserDao;

public class RegServlet extends HttpServlet {
	private static final long serialVersionUID = 5280356329609002908L;
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String username = request.getParameter("username");
		
		String password = request.getParameter("password");
		
		String sex = request.getParameter("sex");
		
		String tel = request.getParameter("tel");
		
		String email = request.getParameter("email");
		
		UserDao userDao = new UserDao();
		if(username != null && !username.isEmpty()){
			if(userDao.userIsExist(username)){
				
				User user = new User();		
				
				user.setUsername(username);	
				user.setPassword(password);
				user.setSex(sex);
				user.setTel(tel);
				user.setEmail(email);
				
				userDao.saveUser(user);	
				request.setAttribute("info", "恭喜，注册成功！<br>");
			}else{
				request.setAttribute("info", "错误：此用户名已存在！");
			}
		}
		// 转发到message.jsp页面
		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

}
