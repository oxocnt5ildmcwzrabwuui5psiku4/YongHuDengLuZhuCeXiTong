package service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.User;
import model.dao.UserDao;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = -3009431503363456775L;
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String username = request.getParameter("username");
		
		String password = request.getParameter("password");
		
		String validationCode = request.getParameter("validation_code");
		
		if(!checkValidationCode(request, validationCode))
        {
			 request.setAttribute("info", "验证码不正确");
	         request.setAttribute("codeError", "验证码不正确");
	         request.getRequestDispatcher("message.jsp").forward(request, response);
        }   
		
		UserDao userDao = new UserDao();	
		
		User user = userDao.login(username, password);
		
		if(user != null){
			
			request.getSession().setAttribute("user", user);
			
			request.getRequestDispatcher("message.jsp").forward(request, response);
		}else{
			
			request.setAttribute("info", "错误：用户名或密码错误！");
			request.getRequestDispatcher("message.jsp").forward(request, response);
		}
	}
	
	protected boolean checkValidationCode(HttpServletRequest request, String validationCode)
    {
        
        String validationCodeSession = (String)request.getSession().getAttribute("validation_code");
        
       
        if(validationCodeSession == null)
        {           
            request.setAttribute("info", "验证码过期");
            request.setAttribute("codeError", "验证码过期");
            return false;
        }
        
        if(!validationCode.equalsIgnoreCase(validationCodeSession))
        {
            request.setAttribute("info", "验证码不正确");
            request.setAttribute("codeError", "验证码不正确");
            return false;
        }
        return true;
    }

}
