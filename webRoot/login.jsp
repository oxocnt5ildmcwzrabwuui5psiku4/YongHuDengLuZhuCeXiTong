<%@ page language="java" contentType="text/html" pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>用户登录</title>
    <link rel="stylesheet" type="text/css" href="CSS/styles.css">
	<script type="text/javascript">		 
		function refresh()
        {
            var img = document.getElementById("img_validation_code")
            img.src = "validation_code?" + Math.random();            
        }
        function checkLogin()
		{
		    var username = document.getElementById("username");

    		// 用户名必须输入
    		if(username.value == "")
    		{
        		alert("必须输入用户名!");
        		username.focus();
        		return;
    		}
    		var password = document.getElementById("password");
   			 // 密码必须输入
   			if(password.value == "")
    		{
        		alert("必须输入密码!");
        		password.focus();
        		return;
    		}
    		
    	
    		var validation_code = document.getElementById("validation_code");
    		
    		if(validation_code.value == "")
    		{
    		    alert("验证码必须输入!");
        		validation_code.focus();
        		return;
    		}
    		
    		// 必须输入验证码
   			login_form.submit();
		}  
        
		 
	</script> 
  </head>
  
  <body>
	  <div align="center">
		  	<div class="div1">
		  		<div >用户登录</div>
		  		<div >
					<div >
				  		<ul>
				  			<li><a href="reg.jsp">用户注册</a></li>
				  			<li><a href="login.jsp">用户登录</a></li>
				  			<li><a href="message.jsp">当前用户</a></li>
				  			<li><a href="UserExitServlet">用户退出</a></li>
				  		</ul>
				  	</div>
				  	 <div class="div3"> 
					    <form action="LoginServlet" method="post" onSubmit="return login(this);">
						    <table align="center" width="300" border="0" class="tb1">
						    	<tr>
						    		<td align="right">用户名：</td>
						    		<td>
						    			<input type="text" name="username">
						    		</td>
						    	</tr>
						    	<tr>
						    		<td align="right">密 码：</td>
						    		<td>
						    			<input type="password" name="password">
						    		</td>
						    	</tr>
						    	<tr>
                                    <td align="right">验证码：</td>
                                    <td>
                                        <input type="text" id="validation_code"  name="validation_code" style="width:60px;margin-top: 2px" size="30" maxlength="30"/>
                                        <img id="img_validation_code" src="validation_code"/>
                                        <input type="button"  value="刷新" onclick="refresh()" />&nbsp;&nbsp;<font color="#FF0000">${requestScope.codeError}</font>
                                    </td>
                                </tr>		    	
          <tr>
						    		<td colspan="2" align="center" height="50">
						    			<input type="submit" value="登 录">
						    			<input type="reset" value="重 置">
						    		</td>
						    	</tr>
						    </table>
						</form>
				  	 </div>
				</div>
		  	</div>
	  </div>
  </body>
</html>
